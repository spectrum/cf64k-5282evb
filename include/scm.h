#ifndef __scm_h
#define __scm_h

struct scm_regs {
	u32 ipsbar;
	u32 rsvd1;
	u32 rambar;
	u32 rsvd2;
	u8 crsr;
	u8 cwcr;
	u8 lpicr;
	u8 cwsr;
};

struct watchdog {
	u16 wcr;
	u16 wmr;
	u16 wcntr;
	u16 wsr;
};

#endif // __scm_h
