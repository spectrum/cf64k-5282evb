#ifndef flash_h
#define flash_h

void init_parallel_flash(void);

void fl_reset(void);
void fl_erase_block(int block);
void fl_erase_chip(void);

void fl_write_binary(volatile unsigned short *src,
                     volatile unsigned short *dst, int size);

int fl_get_manuf_id(void);
int fl_get_dev_id(void);
int fl_check_sector_protected(volatile u16 *address);

#endif /* flash_h */
