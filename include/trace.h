#ifndef __trace_h
#define __trace_h

#define CL_RED		"\x1b[1;31m"
#define CL_GREE		"\x1b[1;32m"
#define CL_YELL		"\x1b[1;33m"
#define CL_BLUE		"\x1b[1;34m"
#define CL_MAG		"\x1b[1;35m"
#define CL_CYAN		"\x1b[1;36m"
#define CL_WHIT		"\x1b[1;37m"
#define CL_RESET	"\x1b[0m"

void log(char *string);
void logi(int value);
void log_hex_buff(char *buff, int size);

void clear_screen(void);

#endif // __trace_h
