#ifndef __gpio_h
#define __gpio_h

#include "arch.h"

#define GPIO_PAR_DSPI0_SIN_DSPI0SIN	(0xC0)
#define GPIO_PAR_DSPI0_SOUT_DSPI0SOUT	(0x30)
#define GPIO_PAR_DSPI0_SCK_DSPI0SCK	(0x0C)

/* General Purpose I/O Module (GPIO) */
struct gpio_regs {
	u8 podr_a;		/* 0x00 */
	u8 podr_b;		/* 0x01 */
	u8 podr_c;		/* 0x02 */
	u8 podr_d;		/* 0x03 */
	u8 podr_e;		/* 0x04 */
	u8 podr_f;		/* 0x05 */
	u8 podr_g;		/* 0x06 */
	u8 podr_h;		/* 0x07 */
	u8 podr_i;		/* 0x08 */
	u8 podr_j;		/* 0x09 */
	u8 podr_k;		/* 0x0A */
	u8 rsvd0;		/* 0x0B */
	u8 pddr_a;		/* 0x0C */
	u8 pddr_b;		/* 0x0D */
	u8 pddr_c;		/* 0x0E */
	u8 pddr_d;		/* 0x0F */
	u8 pddr_e;		/* 0x10 */
	u8 pddr_f;		/* 0x11 */
	u8 pddr_g;		/* 0x12 */
	u8 pddr_h;		/* 0x13 */
	u8 pddr_i;		/* 0x14 */
	u8 pddr_j;		/* 0x15 */
	u8 pddr_k;		/* 0x16 */
	u8 rsvd1;		/* 0x17 */
	u8 ppdsdr_a;		/* 0x18 */
	u8 ppdsdr_b;		/* 0x19 */
	u8 ppdsdr_c;		/* 0x1A */
	u8 ppdsdr_d;		/* 0x1B */
	u8 ppdsdr_e;		/* 0x1C */
	u8 ppdsdr_f;		/* 0x1D */
	u8 ppdsdr_g;		/* 0x1E */
	u8 ppdsdr_h;		/* 0x1F */
	u8 ppdsdr_i;		/* 0x20 */
	u8 ppdsdr_j;		/* 0x21 */
	u8 ppdsdr_k;		/* 0x22 */
	u8 rsvd2;		/* 0x23 */
	u8 pclrr_a;		/* 0x24 */
	u8 pclrr_b;		/* 0x25 */
	u8 pclrr_c;		/* 0x26 */
	u8 pclrr_d;		/* 0x27 */
	u8 pclrr_e;		/* 0x28 */
	u8 pclrr_f;		/* 0x29 */
	u8 pclrr_g;		/* 0x2A */
	u8 pclrr_h;		/* 0x2B */
	u8 pclrr_i;		/* 0x2C */
	u8 pclrr_j;		/* 0x2D */
	u8 pclrr_k;		/* 0x2E */
	u8 rsvd3;		/* 0x2F */
	u16 pcr_a;		/* 0x30 */
	u16 pcr_b;		/* 0x32 */
	u16 pcr_c;		/* 0x34 */
	u16 pcr_d;		/* 0x36 */
	u16 pcr_e;		/* 0x38 */
	u16 pcr_f;		/* 0x3A */
	u16 pcr_g;		/* 0x3C */
	u16 pcr_h;		/* 0x3E */
	u16 pcr_i;		/* 0x40 */
	u16 pcr_j;		/* 0x42 */
	u16 pcr_k;		/* 0x44 */
	u16 rsvd4;		/* 0x46 */
	u8 par_fbctl;		/* 0x48 */
	u8 par_be;		/* 0x49 */
	u8 par_cs;		/* 0x4A */
	u8 par_cani2c;		/* 0x4B */
	u8 par_irqh;		/* 0x4C */
	u8 par_irql;		/* 0x4D */
	u8 par_dspiowh;		/* 0x4E */
	u8 par_dspiowl;		/* 0x4F */
	u8 par_timer;		/* 0x50 */
	u8 par_uart2;		/* 0x51 */
	u8 par_uart1;		/* 0x52 */
	u8 par_uart0;		/* 0x53 */
	u8 par_sdhch;		/* 0x54 */
	u8 par_sdhcl;		/* 0x55 */
	u8 par_simp0h;		/* 0x56 */
	u8 par_simp1h;		/* 0x57 */
	u8 par_ssi0h;		/* 0x58 */
	u8 par_ssi0l;		/* 0x59 */
	u8 par_dbg1h;		/* 0x5A */
	u8 par_dbg0h;		/* 0x5B */
	u8 par_dbgl;		/* 0x5C */
	u8 rsvd5;		/* 0x5D */
	u8 par_fec;		/* 0x5E */
	u8 rsvd6;		/* 0x5F */
	u8 mscr_sdram;		/* 0x60 */
	u8 rsvd7[3];		/* 0x61-0x63 */
	u8 srcr_fb1;		/* 0x64 */
	u8 srcr_fb2;		/* 0x65 */
	u8 srcr_fb3;		/* 0x66 */
	u8 srcr_fb4;		/* 0x67 */
	u8 srcr_dspiow;		/* 0x68 */
	u8 srcr_cani2c;		/* 0x69 */
	u8 srcr_irq;		/* 0x6A */
	u8 srcr_timer;		/* 0x6B */
	u8 srcr_uart;		/* 0x6C */
	u8 srcr_fec;		/* 0x6D */
	u8 srcr_sdhc;		/* 0x6E */
	u8 srcr_simp0;		/* 0x6F */
	u8 srcr_ssi0;		/* 0x70 */
	u8 rsvd8[3];		/* 0x71-0x73 */
	u16 urts_pol;		/* 0x74 */
	u16 ucts_pol;		/* 0x76 */
	u16 utxd_wom;		/* 0x78 */
	u32 urxd_wom;		/* 0x7c */
	u32 hcr1;		/* 0x80 */
	u32 hcr0;		/* 0x84 */
};

#endif //__gpio_h
