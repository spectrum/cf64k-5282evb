/*
 * this file uses U-boot (C) bootloader (www.denx.de) config style
 */
#ifndef __config_h
#define __config_h

#include "arch.h"

/* EVM cou is 66 */
#define CONFIG_SYS_CLK		64000000

#define CONFIG_IPSBAR		0x40000000
#define CONFIG_INIT_RAM_ADDR	0x20000000
#define CONFIG_INIT_RAM_SIZE	0x10000

#define CONFIG_SYS_SDRAM_BASE	0x00000000
#define CONFIG_SYS_SDRAM_SIZE	16

#define CONFIG_SYS_MFD		0x02
#define CONFIG_SYS_RFD		0x00

/*
 * Flash memory bank definition
 */
#define CONFIG_SYS_FLASH_BASE	0xffe00000
#define	CONFIG_SYS_FLASH_MASK	0x001f0000

#define CONFIG_BAUDRATE		115200
#define CONFIG_UART_CONSOLE	0

#define CONFIG_EXEC_FROM_SRAM
#define CONFIG_PLL_SETUP
#define CONFIG_DDR

/* #define CONFIG_SQUARE_WAVE */

#endif // __config_h
