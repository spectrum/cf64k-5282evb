#ifndef __arch_h
#define __arch_h

#include "config.h"

/* CF arch specific */
#define u8 unsigned char
#define u16 unsigned short
#define u32 unsigned long

/* other convenient types */
#define f_bool unsigned char
#define ctrue 1
#define cfalse 0

#define ARCH_UART0		(CONFIG_IPSBAR + 0x00000200)
#define ARCH_UART1              (CONFIG_IPSBAR + 0x00000240)
#define ARCH_UART2              (CONFIG_IPSBAR + 0x00000280)
#define PORTABCD		(CONFIG_IPSBAR + 0x00100000)
#define PORTQS			(CONFIG_IPSBAR + 0x0010000d)
#define DDRABCD			(CONFIG_IPSBAR + 0x00100014)
#define DDRQS			(CONFIG_IPSBAR + 0x00100021)
#define PBCDPAR			(CONFIG_IPSBAR + 0x00100050)
#define PQSPAR			(CONFIG_IPSBAR + 0x00100059)

#define MMAP_SCM		CONFIG_IPSBAR
#define MMAP_CCM		(CONFIG_IPSBAR + 0x00110004)
#define MMAP_WD			(CONFIG_IPSBAR + 0x00140000)
#define MMAP_PLL		(CONFIG_IPSBAR + 0x00120000)
#define MMAP_DDR		(CONFIG_IPSBAR + 0x00000040)
#define MMAP_UART		(CONFIG_IPSBAR + 0x00000200)
#define MMAP_PM			(CONFIG_IPSBAR + 0x00000010)

#define CSAR0			(CONFIG_IPSBAR + 0x00000080)
#define CSCR0			(CONFIG_IPSBAR + 0x0000008a)
#define CSMR0			(CONFIG_IPSBAR + 0x00000084)

#define CSAR1			(CONFIG_IPSBAR + 0x0000008c)

#define MCFSDRAMC_DCR_RTIM_6	(1 << 9)
#define MCFSDRAMC_DCR_RC(x)	((x) & 0x1ff)
#define MCFSDRAMC_DACR_BASE(x)	((x) & 0xfffc0000)
#define MCFSDRAMC_DACR_CASL(x)	(((x) & 0x03) << 12)
#define MCFSDRAMC_DACR_CBM(x)	(((x) & 0x07) << 8)
#define MCFSDRAMC_DACR_PS_32	0x00000000
#define MCFSDRAMC_DMR_V		0x00000001
#define MCFSDRAMC_DACR_IP	0x00000008
#define MCFSDRAMC_DACR_RE	0x00008000
#define MCFSDRAMC_DACR_IMRS	0x00000040

#define MCFCLOCK_SYNCR_MFD(x)	(((x) & 0x0007) << 12)
#define MCFCLOCK_SYNCR_RFD(x)	(((x) & 0x0007) << 8)
#define MCFCLOCK_SYNSR_LOCK	0x08

#define MCFGPIO_PASPAR		(CONFIG_IPSBAR + 0x00100056)
#define MCFGPIO_PUAPAR		(CONFIG_IPSBAR + 0x0010005C)

#define CS_CSCR_WS(x)		((x) << 10)
#define CS_CSCR_AA		(1 << 8)
#define CS_CSCR_PS16		(2 << 6)

#define UART_BASE		(CONFIG_IPSBAR + 0x000200)
#define UART_MOD_SIZE		0x40

#define MMAP_GPIO		0


#define ICACHE_STATUS           (CONFIG_INIT_RAM_ADDR + \
				 CONFIG_INIT_RAM_SIZE - 8)
#define DCACHE_STATUS           (CONFIG_INIT_RAM_ADDR + \
				 CONFIG_INIT_RAM_SIZE - 4)

#define CONFIG_SYS_ICACHE_INV   (CF_CACR_BCINVA + CF_CACR_ICINVA)
#define CONFIG_SYS_DCACHE_INV   (CF_CACR_DCINVA)


#define CONFFG_SYS_CACHE_ICACR	(CF_CACR_CENB | CF_CACR_DISD | \
                                 CF_CACR_CEIB | CF_CACR_DBWE | \
                                 CF_CACR_EUSP)
#define CONFIG_SYS_CACHE_DCACR	CONFFG_SYS_CACHE_ICACR

#define CONFIG_SYS_CACHE_ACR0	(0x40000000 | \
				 CF_ADDRMASK(128) | \
				 CF_ACR_EN | CF_ACR_SM_ALL)

#define CONFIG_SYS_CACHE_ACR1	(0x40000000 | \
				 CF_ADDRMASK(128) | \
				 CF_ACR_EN | CF_ACR_SM_ALL)

#endif /* __arch_h */
