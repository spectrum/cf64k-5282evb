#ifndef __cache_h
#define __cache_h

#define CF_ADDRMASK(x)		(((x > 0x10) ? ((x >> 4) - 1) : (x)) << 16)
#define CF_CACR_CENB		(1 << 31)
#define CF_CACR_CPD		(1 << 28)
#define CF_CACR_CFRZ		(1 << 27)
#define CF_CACR_CINV		(1 << 24)
#define CF_CACR_DISI		(1 << 23)
#define CF_CACR_DISD		(1 << 22)
#define CF_CACR_INVI		(1 << 21)
#define CF_CACR_INVD		(1 << 20)
#define CF_CACR_CEIB		(1 << 10)
#define CF_CACR_DCM		(1 << 9)
#define CF_CACR_DBWE		(1 << 8)
#define CF_CACR_DWP		(1 << 5)
#define CF_CACR_EUSP		(1 << 4)
#define CF_CACR_CLNF		(3 << 4)

/* ***** ACR ***** */
#define CF_ACR_ADR_UNMASK	(0x00FFFFFF)
#define CF_ACR_ADR(x)		((x & 0xFF) << 24)
#define CF_ACR_ADRMSK_UNMASK	(0xFF00FFFF)
#define CF_ACR_ADRMSK(x)	((x & 0xFF) << 16)
#define CF_ACR_EN		(1 << 15)
#define CF_ACR_SM_UNMASK	(0xFFFF9FFF)
#define CF_ACR_SM_UM		(0 << 13)
#define CF_ACR_SM_SM		(1 << 13)
#define CF_ACR_SM_ALL		(3 << 13)
#define CF_ACR_WP		(1 << 2)

void dcache_enable(void);
void dcache_disable(void);

#endif /* __cache_h */
