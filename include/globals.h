#ifndef __globals_h
#define __globals_h

/*
 * code below copy useful functions form U-boot (C) bootloader (www.denx.de)
 */
#include "arch.h"

#define __BIG_ENDIAN

/*
 * 8, 16 and 32 bit, big and little endian I/O operations, with barrier.
 */
static inline int in_8(volatile u8 *addr)
{
	return (int)*addr;
}

static inline void out_8(volatile u8 *addr, int val)
{
	*addr = (u8)val;
}

static inline int in_be16(volatile u16 *addr)
{
	return (*addr & 0xFFFF);
}

static inline void out_be16(volatile u16 *addr, int val)
{
	*addr = (u16)val;
}

static inline unsigned in_be32(volatile u32 *addr)
{
	return (*addr);
}

static inline void out_be32(volatile u32 *addr, int val)
{
	*addr = val;
}

#define readb(addr)		in_8((volatile u8 *)(addr))
#define writeb(b,addr)		out_8((volatile u8 *)(addr), (b))
#if !defined(__BIG_ENDIAN)
#define readw(addr)		(*(volatile u16 *) (addr))
#define readl(addr)		(*(volatile u32 *) (addr))
#define writew(b,addr)		((*(volatile u16 *) (addr)) = (b))
#define writel(b,addr)		((*(volatile u32 *) (addr)) = (b))
#else
#define readw(addr)		in_be16((volatile u16 *)(addr))
#define readl(addr)		in_be32((volatile u32 *)(addr))
#define writew(b,addr)		out_be16((volatile u16 *)(addr),(b))
#define writel(b,addr)		out_be32((volatile u32 *)(addr),(b))
#endif

/*
 * Clear and set bits in one shot. These macros can be used to clear and
 * set multiple bits in a register using a single call. These macros can
 * also be used to set a multiple-bit bit pattern using a mask, by
 * specifying the mask in the 'clear' parameter and the new bit pattern
 * in the 'set' parameter.
 */
#define clrbits(type, addr, clear) \
	out_##type((addr), in_##type(addr) & ~(clear))

#define setbits(type, addr, set) \
	out_##type((addr), in_##type(addr) | (set))

#define clrsetbits(type, addr, clear, set) \
	out_##type((addr), (in_##type(addr) & ~(clear)) | (set))

#define setbits_8(addr, set) setbits(8, addr, set)
#define clrsetbits_8(addr, clear, set) clrsetbits(8, addr, clear, set)

#define clrbits_be16(addr, clear) clrbits(be16, addr, clear)
#define setbits_be16(addr, set) setbits(be16, addr, set)
#define clrsetbits_be16(addr, clear, set) clrsetbits(be16, addr, clear, set)

#define clrbits_be32(addr, clear) clrbits(be32, addr, clear)
#define setbits_be32(addr, set) setbits(be32, addr, set)
#define clrsetbits_be32(addr, clear, set) clrsetbits(be32, addr, clear, set)

void init_arch_globals(void);

typedef struct gtloba_data {
	u32 inp_clk;
	u32 vco_clk;
	u32 cpu_clk;
	u32 flb_clk;
	u32 bus_clk;
	u32 ram_size;
} gd_t;

int c_str_length(u8* string);

#endif // __globals_h
