#ifndef __ccm_h
#define __ccm_h

#include "arch.h"

/* Chip Configuration Module (CCM) */
typedef struct ccm_regs {
	u16 ccr;		/* Chip Configuration */
	u16 res1;
	u16 rcon;		/* Reset Configurtation */
	u16 cir;		/* Chip Identification */
	u16 res2;
	u16 misccr;		/* Miscellaneous Control */
	u16 cdrh;		/* Clock Divider */
	u16 cdrl;		/* Clock Divider */
	u16 uocsr;		/* USB On-the-Go Controller Status */
	u16 uhcsr;
	u16 misccr3;
	u16 misccr2;
	u16 adctsr;
	u16 dactsr;
	u16 sbfsr;
	u16 sbfcr;
	u32 fnacr;
} ccm_t;

void ccm_disable_limp_mode(void);
void ccm_set_flexbus_half_clock(f_bool half);
f_bool ccm_is_half_flexbus_clock(void);

#endif // __ccm_h
