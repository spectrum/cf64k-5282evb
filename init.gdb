#
# GDB Init script for the Coldfire processor.
#

define hook-quit
	set confirm off
end

# multilink
target remote localhost:3333
# bdm (old)
# target remote | m68k-bdm-gdbserver pipe /dev/tblcf0


# load file now only, once regs and rambar are initialized

set {int}0x40000000 = 0x40000001
set $rambar0 = 0x22
set {int}0x40000008 = 0x20000221
set $vbr = 0x00000000

load bin/cf64k.elf
file bin/cf64k.elf

set $pc=_start

set print pretty
set print asm-demangle
display/i $pc
select-frame 0

continue
