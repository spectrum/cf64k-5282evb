/*
 * pll.c
 *
 * mcf54416 sram bootloader ccm
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "config.h"
#include "globals.h"
#include "ccm.h"
#include "pll.h"

#include <stdint.h>

extern gd_t gd;

/*
 * if not from sram, settings are bootstrap settings or sbf settings
 */
void pll_initial_setup(void)
{
	struct pll_regs *pll = (struct pll_regs *)MMAP_PLL;

	/*
	 * Set speed /PLL :
	 * for M5282EVB we have a 8MHZ crystal.
	 * so 8 (MFD/RFD) x 8 = 64
	 */
	out_be16(&pll->syncr,
		 MCFCLOCK_SYNCR_MFD(CONFIG_SYS_MFD) |
		 MCFCLOCK_SYNCR_RFD(CONFIG_SYS_RFD));

	while (!(in_8(&pll->synsr) & MCFCLOCK_SYNSR_LOCK))
		;

	*(volatile unsigned char *)PBCDPAR = 0xc0;
}

int init_pll_clocks(void)
{
	gd.cpu_clk = CONFIG_SYS_CLK;
	/* should be ok for 5282 */
	gd.bus_clk = gd.cpu_clk;

	pll_initial_setup();
}
