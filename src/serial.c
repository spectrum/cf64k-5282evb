/*
 * serial.c
 *
 * mcf54416 sram bootloader serial driver
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Note: most of the code and related defines comes from
 *       the U-boot bootloader (www.denx.de).
 */

#include "globals.h"
#include "serial.h"
#include "config.h"
#include "gpio.h"
#include "power.h"

#define GETCHAR_TIMEOUT_TICKS_MAX	0x200000

extern gd_t gd;

void uart_port_conf(int port)
{
	volatile u8 *puapar = (volatile u8 *)MCFGPIO_PUAPAR;
	volatile u16 *paspar = (volatile u16 *)MCFGPIO_PASPAR;

	/* Setup Ports: */
	switch (port) {
	case 0:
		*puapar &= 0xfc;
		*puapar |= 0x03;
		break;
	case 1:
		*puapar &= 0xf3;
		*puapar |= 0x0c;
		break;
	case 2:
		*paspar &= 0xff0f;
		*paspar |= 0x00a0;
		break;
	}
}

static struct uart_regs *get_uart_address(int port_idx)
{
	return (struct uart_regs *)(MMAP_UART + (port_idx * 0x40));
}

void init_serial_port(u8 port_idx)
{
	struct uart_regs *uart = get_uart_address(port_idx);
	u32 divider;
	u32 baudrate;

	uart_port_conf(port_idx);

	baudrate = CONFIG_BAUDRATE;

	/* write to SICR: SIM2 = uart mode,dcd does not affect rx */
	out_8(&uart->ucr, UART_UCR_RESET_RX);
	out_8(&uart->ucr, UART_UCR_RESET_TX);
	out_8(&uart->ucr, UART_UCR_RESET_ERROR);
	out_8(&uart->ucr, UART_UCR_RESET_MR);
	__asm__("nop");

	out_8(&uart->uimr, 0);

	/* write to CSR: RX/TX baud rate from timers */
	out_8(&uart->ucsr, UART_UCSR_RCS_SYS_CLK | UART_UCSR_TCS_SYS_CLK);

	out_8(&uart->umr1, UART_UMR_BC_8 | UART_UMR_PM_NONE);
	out_8(&uart->umr2, UART_UMR_SB_STOP_BITS_1);

	/*
	 * baudrate = fsys / (32 * divider)
	 * divider = baudrate / fsys
	 */
	/* Setting up BaudRate, using u-boot correction  */
	divider = gd.cpu_clk / (baudrate * 32);
	divider++;

	/* write to CTUR: divide counter upper byte */
	out_8(&uart->ubg1, (u8)((divider & 0xff00) >> 8));
	/* write to CTLR: divide counter lower byte */
	out_8(&uart->ubg2, (u8)(divider & 0x00ff));

	out_8(&uart->ucr, UART_UCR_RX_ENABLED | UART_UCR_TX_ENABLED);
}

void serial_putc(unsigned char c)
{
	struct uart_regs *uart = get_uart_address(CONFIG_UART_CONSOLE);

	if (c == '\n')
		serial_putc('\r');

	/* Wait for last character to go. */
	while (!(readb(&uart->usr) & UART_USR_TXRDY))
		;

	writeb(c, &uart->utb);
}

int serial_getchar(int timeout)
{
	struct uart_regs *uart = get_uart_address(CONFIG_UART_CONSOLE);
	int ticks = 0;

	/* Wait for a character to arrive. */

	if (timeout) {
		while (!(readb(&uart->usr) & UART_USR_RXRDY)) {
			if (ticks++ > GETCHAR_TIMEOUT_TICKS_MAX)
				return GETCHAR_TIMEOUT;
		}
		return readb(&uart->urb);
	} else {
		while (!(readb(&uart->usr) & UART_USR_RXRDY))
			;
	}

	return readb(&uart->urb);
}

void serial_write(u8 *buff)
{
	int size = c_str_length(buff);

	while(size--)
		serial_putc(*buff++);
}
