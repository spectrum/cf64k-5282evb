/*
 * main.c
 *
 * mcf54416 sram bootloader main start
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "init.h"
#include "serial.h"
#include "trace.h"
#include "utils.h"
#include "version.h"
#include "flash.h"

#include "memory.h"

#include <stdint.h>

#define sectsize (1024 * 4)

extern void ddr_init(void);

int get_binary(void)
{
	volatile uint8_t *ptrr = (volatile uint8_t *)0x00000000;
	int i = 0, kb = 0, c;

	log("\033[2J\033[HBinary upload\r\n\r\n");
	log("Waiting ... \r\n");
	log("\x1b[s");

	while (i == 0) {
		while ((c = serial_getchar(1)) != GETCHAR_TIMEOUT) {
			ptrr[i++] = c;
			/* no write of info here ! or chars are lost */
		}
	}

	log("\r\n\r\nfile uploaded successfully\r\n");
	log("received ");
	logi(i);
	log(" bytes\r\n\r\n");
	log("write to flash (y/n) ? ");

	char x = serial_getchar(0);

	if (x == 'y')
		return i;

	log("\r\naborted\r\n");

	serial_getchar(0);

	return 0;
}

static void dump_flash(void)
{
	unsigned short i, q;
	volatile u16 *base = (volatile u16 *)CONFIG_SYS_FLASH_BASE;
	volatile u16 *sa;

	sa = base;

	serial_write("Manufacturer : ");
	serial_write(zpad(itohex(fl_get_manuf_id(), 0), 4));
	serial_putc('\n');
	serial_write("Device id    : ");
	serial_write(zpad(itohex(fl_get_dev_id(), 0), 4));
	serial_write("\n\n");

	serial_write("Sector protected check : ");
	serial_write(zpad(itohex(fl_check_sector_protected(sa), 0), 2));
	serial_write("\n\n");

	fl_reset();

	for (i = 0; i < 256; i += 8) {
		serial_write(zpad(itohex((int)&base[i], 0), 8));
		serial_write(": ");
		for (q = 0; q < 8; ++q) {
			serial_write(zpad(itohex(base[i + q] & 0xff, 0), 2));
			serial_putc(' ');
			serial_write(zpad(itohex(base[i + q] >> 8, 0), 2));
			serial_putc(' ');
		}
		serial_putc('\n');
	}

	log(CL_YELL "Done.\n");
	serial_getchar(0);
}

/*
 * release mode only
 */
static void erase_flash(void)
{
	unsigned short x, y;

	log(CL_YELL "Erasing chip ...\n");
	fl_erase_chip();

	log(CL_YELL "Done.\n");
	serial_getchar(0);
}

/**
 * Erase and write bootloader in first 64K block.
 */
void write_bootloader(void)
{
	int size;

	fl_erase_block(0);
	fl_erase_block(1);
	fl_erase_block(2);
	fl_erase_block(3);
	fl_erase_block(4);

	size = get_binary();

	if (size > 0) {
		fl_write_binary(0,
			(unsigned short *)CONFIG_SYS_FLASH_BASE, size);
	}

	log(CL_YELL "Done.\n");
	serial_getchar(0);
}

int __main(void)
{
	int i, ch;

	system_init();

	for (;;) {
		clear_screen();
		log(CL_YELL "Greetings !\n");
		log(CL_CYAN "cf64 mcf52x2 bootloader v. "
			CL_GREE version CL_WHIT "\r\n\r\n");

		log(CL_YELL "1 ..." CL_GREE " erase flash\n");
		log(CL_YELL "2 ..." CL_MAG
			" write binary to flash, part. u-boot\n");
		log(CL_YELL "3 ..." CL_MAG " dump flash\n");
		log(CL_YELL "6 ..." CL_CYAN " sdram memtest\n\n");
		log(CL_YELL "please select ...\n\n");

		ch = serial_getchar(0);
		ch -= 48;

		switch (ch) {
		case 1:
			erase_flash();
			break;
		case 2:
			write_bootloader();
			break;
		case 3:
			dump_flash();
			break;
		case 4:
			break;
		case 6:
			mem_test();
			break;
		}
	}

	return 0;
}
