/*
 * init.c
 *
 * mcf528x initializations
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Code comes from u-boot - Freescale (C)
 *
 */

#include "globals.h"
#include "arch.h"

extern gd_t gd;

struct ddr_regs {
	u16 dcr;
	u16 res;
	u32 res2;
	u32 dacr0;
	u32 dmr0;
	u32 dacr1;
	u32 dmr1;
};

/*
 * EVB has 16M (2x MT48LC4M16A2TG)
 */
int init_dram(void)
{
	struct ddr_regs *ddr = (struct ddr_regs *)MMAP_DDR;
	u32 dramsize, i, dramclk, dacr0;

	dramsize = CONFIG_SYS_SDRAM_SIZE * 1024 * 1024;
	for (i = 0x13; i < 0x20; i++) {
		if (dramsize == (1 << i))
			break;
	}
	i--;

	dacr0 = in_be32(&ddr->dacr0);

	if (dacr0 & MCFSDRAMC_DACR_RE)
		return -1;

	dramclk = gd.bus_clk / 1000000;

	out_be16(&ddr->dcr, MCFSDRAMC_DCR_RTIM_6 | MCFSDRAMC_DCR_RC((15 * dramclk) >> 4));
	asm volatile("nop");

	out_be32(&ddr->dacr0, MCFSDRAMC_DACR_BASE(CONFIG_SYS_SDRAM_BASE)
				| MCFSDRAMC_DACR_CASL(1)
				| MCFSDRAMC_DACR_CBM(3)
				| MCFSDRAMC_DACR_PS_32);
	asm volatile("nop");

	out_be32(&ddr->dmr0, (dramsize - 1) & 0xfffc0000 | MCFSDRAMC_DMR_V);

	/* Set IP (bit 3) in DACR */
	setbits_be32(&ddr->dacr0, MCFSDRAMC_DACR_IP);
	asm volatile("nop");

	/* Wait 30ns to allow banks to precharge */
	for (i = 0; i < 5; i++)
		asm volatile("nop");

	/*
	 * Write to this block to initiate precharge,
	 * need to be in asm, compiler does not like writing to 0, it
	 * coimpile into a trap.
	 */
	asm volatile(
		"mov.l	#0xdeadbeef, %d0	\n"
		"mov.l	#0, %a0			\n"
		"mov.l	%d0, (%a0)		\n"
	);

	/* Set RE (bit 15) in DACR */
	setbits_be32(&ddr->dacr0, MCFSDRAMC_DACR_RE);
	asm volatile("nop");

	/* Wait for at least 8 auto refresh cycles to occur */
	for (i = 0; i < 2000; i++)
		asm volatile("nop");

	/* Finish the configuration by issuing the IMRS. */
	setbits_be32(&ddr->dacr0, MCFSDRAMC_DACR_IMRS);
	asm volatile("nop");

	/* Write to the SDRAM Mode Register */
	*(volatile u32 *)(CONFIG_SYS_SDRAM_BASE + 0x400) = 0xa5a59696;

	gd.ram_size = dramsize;

	return 1;
}
