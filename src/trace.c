/*
 * trace.c
 *
 * mcf54416 sram bootloader trace utils
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "trace.h"
#include "serial.h"
#include "utils.h"

void clear_screen(void)
{
	serial_write("\x1b[2J\x1b[1;1H");
}

void log(char *string)
{
	serial_write(string);
}

void logi(int n)
{
	serial_write(itoa(n));
}

void log_hex_buff(char *buff, int size)
{
	int i, q;
	u8 c;

	for(i = 0; i < size; i += 16) {
		log(CL_YELL);
		log("0x");
		log(zpad(itohex(i, 0), 4));
		log(CL_WHIT ": ");
		for (q = 0; q < 16 && (i + q) < size; ++q) {
			c = *buff++ & 0xff;
			if (c <= 32) log(CL_CYAN); else log(CL_GREE);
			log(zpad(itohex(c, 0), 2));
			log(" ");
		}
		log(CL_RESET);
		log("\r\n");
	}
}

void exception(void)
{
	clear_screen();

	serial_write(CL_RED "+++ EXCEPTION ERROR\n" CL_RESET);
}
