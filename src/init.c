/*
 * init.c
 *
 * mcf528x initializations
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "arch.h"
#include "globals.h"
#include "serial.h"
#include "scm.h"
#include "ddr.h"
#include "pll.h"
#include "flash.h"
#include "trace.h"

static void init_cpu(void)
{
	struct scm_regs *scm = (struct scm_regs *)MMAP_SCM;
	struct watchdog *wd = (struct watchdog *)MMAP_WD;

	/* Disable core watchdog */
	out_8(&scm->cwcr, 0);
	out_be16(&wd->wcr, 0);
}

void system_init(void)
{
	init_cpu();
	init_arch_globals();
	init_pll_clocks();
	/* moving flash out of the way (CS0 -> to higher addr) */
	init_parallel_flash();
	init_dram();
	init_serial_port(CONFIG_UART_CONSOLE);
}
