/*
 * utils.c
 *
 * mcf54416 sram bootloader utils
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "utils.h"

#define MAX_OPT_STRING  256
static char ptemp[MAX_OPT_STRING];

void memset(char *off, char c, int len)
{
	while (len--) *off++ = c;
}

int strlen(char *str)
{
	int len = 0;

	while (*str++ != 0) len++;

	return len;
}

char * itoa(int val)
{
	char *p = ptemp + (MAX_OPT_STRING - 1);
	char sign_m = 0;

	*p-- = 0;

	if (val < 0) {
		val = - val;
		sign_m++;
	}

	do {
		*p-- = (val % 10) + '0'; val /= 10;
	} while(val);

	if (sign_m) *p-- = '-';

	return ++p;
}

char * itohex(int val, unsigned char caps)
{
	char *p = ptemp + (MAX_OPT_STRING - 1);
	unsigned char v;
	unsigned int uval = (unsigned int)val;

        *p-- = 0;
        do {
		v = uval % 16;
		if (v > 9)
			v += (caps) ? 55 : 87;
		else
			v += 0x30;

		*p-- = v;
		uval >>= 4;
	} while(uval);

	return ++p;
}

/*
 * there must be array allocated backward (previous ptemp usage assumed)
 */
char * zpad(char *sz, int times)
{
	times -= strlen(sz);
	while (times--) {*--sz = '0';}

	return sz;
}
