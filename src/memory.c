/*
 * memory.c
 *
 * mcf54416 ddr init
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * gasc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Note: most of the code and related defines comes from
 *       the U-boot bootloader (www.denx.de).
 */

#include "trace.h"
#include "serial.h"
#include "utils.h"
#include "cache.h"

static void read_2M_block(volatile unsigned long *offs)
{
	int i;

	for (i = 0; i < (1024 * 512); i++) {
		if (*offs++ != i) {
			log("\r\nerror :( i = ");
			log(itoa(i));
			log("\r\n *offs = ");
			log(itoa(*(offs - 1)));
			log("\r\n\r\n");
			break;
		}
	}
}

void mem_test(void)
{
	unsigned int i, q;
	volatile unsigned long *t, *offs;

	clear_screen();

	log("step 1: test write + read back\r\n");
	log("(each dot are 2 MB)\r\n");

	for (q = 0; q < 8; ++q) {
		offs = (volatile unsigned long *)
			(0x00000000 + q * (1024 * 1024 * 2));
		t = offs;
		for (i = 0; i < (1024 * 512); i++) {
			*t++ = i;
		}
		read_2M_block(offs);
		log(".");
	}
	log("\nstep 1: teste passed \\o/\n");
	log("step 2: simple read\n");

	/* re read all */
	for (q = 0; q < 8; ++q) {
		offs = (volatile unsigned long *)
			(0x00000000 + q * (1024 * 1024 * 2));
		read_2M_block(offs);

		log(".");
	}
	log("\nstep 2: simple read passed \\o/\n");
	log("step 3: dcache on read\n");

	dcache_disable();

	/* re read all */

	for (q = 0; q < 8; ++q) {
		offs = (volatile unsigned long *)
			(0x00000000 + q * (1024 * 1024 * 2));
		read_2M_block(offs);
		log(".");
	}

	dcache_enable();

	log("\nsdram test step 2 passed \\o/\n");
exit:
	log("press a key\n");

	serial_getchar(0);
}
