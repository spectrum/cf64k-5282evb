/*
 * ccm.c
 *
 * mcf54416 sram bootloader ccm
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "globals.h"
#include "ccm.h"

void ccm_set_flexbus_half_clock(f_bool half)
{
	struct ccm_regs *ccm = (struct ccm_regs *)MMAP_CCM;

	if (half)
		setbits_be16(&ccm->misccr2, 0x02);
	else
		clrbits_be16(&ccm->misccr2, 0x02);
}

f_bool ccm_is_half_flexbus_clock(void)
{
	struct ccm_regs *ccm = (struct ccm_regs *)MMAP_CCM;

	return (in_be16(&ccm->misccr2) & 2);
}

void ccm_disable_limp_mode(void)
{
	struct ccm_regs *ccm = (struct ccm_regs *)MMAP_CCM;

	/* disable limp mode */
	clrbits_be16(&ccm->misccr, (1 << 12));
}
