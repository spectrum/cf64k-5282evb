/*
 * flash.c
 *
 * mcf528x parallel flash initializations
 *
 * Copyright 2016 Angelo Dureghello - Sysam <angelo@sysam.it>
 *
 * This file is part of cf64k firmware application.
 *
 * cf64k is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cf64k is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cf64k.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Code comes from u-boot - Freescale (C)
 *
 */

#include "globals.h"
#include "arch.h"

volatile u16 *baseaddress;

#define SECT_SIZE		1024
#define BLOCK_SIZE		65536

#define FLASH_RESET		0xf0f0
#define FLASH_READ_MODE		0xF0

struct cs_regs {
	u16 csar;
	u16 res;
	u32 csmr;
	u16 res2;
	u16 cscr;
};

static void delay(int size)
{
	unsigned int  i;

	for (i = 0; i < size; i++)
		asm("nop");
}

static inline void flash_unlock_seq()
{
	*(baseaddress + 0x555) = 0xaa;
	*(baseaddress + 0x2AA) = 0x55;
}

static inline void flash_reset(void)
{
	flash_unlock_seq();

	*(baseaddress + 0x000) = 0xf0;
}


static void flash_erase_status_poll(volatile unsigned short *dest)
{
	unsigned short ready;

	/* Wait for verification ... */
	while (1) {
		ready = (*dest & (1 << 7));
		/* Good burn! */
		if (ready)
			break;

		delay(10);
	}
}

void fl_erase_block(int block)
{
	unsigned long addr = 0x10000 * block / 2;

	/* Disable interrupts */
	asm volatile ("move #0x2700, %sr");

	/* Erase block (64K) */
	flash_unlock_seq();
	*(baseaddress + 0x555) = 0x80;
	flash_unlock_seq();
	*(baseaddress + addr) = 0x30;

	/* Re-enable interrupts */
	asm volatile ("move #0x2000, %sr");

	flash_erase_status_poll(baseaddress + addr);
}

/*
 * AMD STANDARD WRITE
 */
void fl_write_word(volatile unsigned short *dest, unsigned short value)
{
	unsigned char ready;

	/*
	 * value = 0x0400; significa D26 (pijn 112) settato su cpu
	 */
	asm volatile ("move #0x2700, %sr");

	flash_unlock_seq();
	*(baseaddress + 0x555) = 0xa0;
	*dest = value;

	/* Re-enable interrupts */
	asm volatile ("move #0x2000, %sr");

	/* Wait for verification ... */
	while (1) {
		ready = *dest == value;
		/* Good burn! */
		if (ready)
			break;

		delay(10);
	}
}

int fl_get_manuf_id(void)
{
	int id;

	flash_unlock_seq();
	*(baseaddress + 0x555) = 0x90;
	id = *baseaddress;

	return id;
}

int fl_get_dev_id(void)
{
	int id;

	flash_unlock_seq();
	*(baseaddress + 0x555) = 0x90;
	id = *(baseaddress + 1);

	return id;
}

void fl_reset(void)
{
	*baseaddress = FLASH_RESET;
	*baseaddress = FLASH_RESET;
}

void fl_write_binary(volatile unsigned short *src,
		     volatile unsigned short *dst, int size)
{
	int i;

	/* Reset flash */
	*baseaddress = FLASH_RESET;

	/* Write 16bit words */
	size >>= 1;
	while (size--)
		fl_write_word(dst++, *src++);

	/* Back in read mode */
	*(unsigned short*)baseaddress = FLASH_READ_MODE;
}

int fl_check_sector_protected(volatile u16 *address)
{
	flash_unlock_seq();
	*(baseaddress + 0x555) = 0x90;

	return *(address + 2);
}

void fl_erase_chip(void)
{
	/* Disable interrupts */
	asm volatile ("move #0x2700, %sr");

	/* Erase chip */
	flash_unlock_seq();
	*(baseaddress + 0x555) = 0x80;
	flash_unlock_seq();
	*(baseaddress + 0x555) = 0x10;

	/* Re-enable interrupts */
	asm volatile ("move #0x2000, %sr");

	flash_erase_status_poll((volatile unsigned short *)0xffe00000);
}

void init_parallel_flash(void)
{
	struct cs_regs *cs0 = (struct cs_regs *)CSAR0;

	/* Setup CS0 for the flash */
	out_be16(&cs0->csar, CONFIG_SYS_FLASH_BASE >> 16);
	out_be16(&cs0->cscr, CS_CSCR_WS(6) | CS_CSCR_AA | CS_CSCR_PS16);
	out_be32(&cs0->csmr, (CONFIG_SYS_FLASH_MASK + 1));

	baseaddress = (volatile u16 *)CONFIG_SYS_FLASH_BASE;

	flash_reset();
}
