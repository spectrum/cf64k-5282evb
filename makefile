
# GCC_EXEC_PREFIX=/opt/toolchains/m68k/gcc-10.1.0-nolibc/m68k-linux/bin/m68k-linux-  DO NOT USE, errors in aligmnets

# OK for opencf, keep -O0
# Works properly

# seems to fail producing opcodes as movel %d0 as 0x2039 40000048
# 2000057e <init_dram>:
# 2000057e:       2039 4000 0048  movel 40000048 <_stack_start+0x1fff0048>,%d0
# GCC_EXEC_PREFIX=/opt/toolchains/m68k/m68k-sysam-uclinux-uclibc/bin/m68k-sysam-uclinux-uclibc-
# Toolchains to be tested
GCC_EXEC_PREFIX=/opt/toolchains/m68k/gcc-13.2.0-nolibc/m68k-linux/bin/m68k-linux-
# GCC_EXEC_PREFIX=/opt/toolchains/m68k/m68k-openadk-linux-uclibc/toolchain_generic-m68k_uclibc-ng_cf54415/usr/bin/m68k-openadk-linux-uclibc-
# GCC_EXEC_PREFIX=/opt/toolchains/m68k/freescale-coldfire-2011.09/bin/m68k-linux-gnu-

# compile time configs
CPU=5282

CC=$(GCC_EXEC_PREFIX)gcc
LD=$(GCC_EXEC_PREFIX)ld
OBJCOPY=$(GCC_EXEC_PREFIX)objcopy
OBJDUMP=$(GCC_EXEC_PREFIX)objdump

BINDIR=bin
OBJDIR=obj
SRCDIR=src
INCDIR=include
BINARY=$(BINDIR)/cf64k

INC=-I$(INCDIR)

CFLAGS=-pipe -mcpu=5282 -m528x -DCONFIG_MCF5282 -fno-builtin

SRCS:= $(wildcard $(SRCDIR)/*.c)
ASMS:= $(wildcard $(SRCDIR)/*.S)
OBJS:= $(patsubst %.c,%.o,$(SRCS)) $(patsubst %.S,%.o,$(ASMS))
OBJS:= $(patsubst $(SRCDIR)%,$(OBJDIR)%,$(OBJS))

all: CFLAGS+= -O2 -ffreestanding -nostdlib -ffunction-sections -fdata-sections -ffixed-d7
# all: CFLAGS+=-O0 -ffreestanding -nostdlib
all: $(BINARY)

debug: CFLAGS+=-O0 -g
debug: $(BINARY)

$(BINARY): $(BINARY).elf
	$(OBJCOPY) -O binary $(BINARY).elf $@
	$(OBJDUMP) -D -m$(CPU) $(BINARY).elf > $(BINARY).lst

$(BINARY).elf: $(OBJS)
	$(LD) -n -Bstatic -T ram.ld -o $(BINARY).elf $(OBJS) -Map $(BINARY).map

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $(INC) $(CONFIG) -c -o $@ $<

$(OBJDIR)/%.o: $(SRCDIR)/%.S
	$(CC) $(CFLAGS) $(INC) $(CONFIG) -c -o $@ $<

clean:
	rm obj/*.o
	rm bin/*
